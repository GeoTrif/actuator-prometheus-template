package com.actuatorprometheustemplate.bootstrap;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DataLoader {

    public List<Integer> getCounterIdsList() {
        List<Integer> counterIds = new ArrayList<>();
        counterIds.add(1);
        counterIds.add(2);
        counterIds.add(3);
        counterIds.add(4);
        counterIds.add(5);

        return counterIds;
    }
}
