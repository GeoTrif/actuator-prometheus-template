package com.actuatorprometheustemplate.service;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.stereotype.Service;

@Service
public class ActuatorPrometheusService {

    private static final String INCREMENT_COUNTER_SUCCESS_METRIC = "increment.counter.success";
    private static final String COUNTER_ID_TAG = "counterId";

    private final MeterRegistry meterRegistry;

    public ActuatorPrometheusService(final MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    public void incrementCounterMetric(final Integer counterId) {
        Counter.builder(INCREMENT_COUNTER_SUCCESS_METRIC)
                .tag(COUNTER_ID_TAG, counterId.toString())
                .register(meterRegistry).increment();
    }
}
