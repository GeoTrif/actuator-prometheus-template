package com.actuatorprometheustemplate.controller;

import com.actuatorprometheustemplate.bootstrap.DataLoader;
import com.actuatorprometheustemplate.service.ActuatorPrometheusService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/actuator-prometheus-test")
public class ActuatorPrometheusController {

    private final ActuatorPrometheusService actuatorPrometheusService;
    private final DataLoader dataLoader;

    public ActuatorPrometheusController(final ActuatorPrometheusService actuatorPrometheusService, final DataLoader dataLoader) {
        this.actuatorPrometheusService = actuatorPrometheusService;
        this.dataLoader = dataLoader;
    }

    @GetMapping
    public void incrementCounterForPrometheusTemplate(@RequestParam final Integer counterId) {
        actuatorPrometheusService.incrementCounterMetric(counterId);
    }
}
